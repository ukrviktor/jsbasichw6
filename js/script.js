// Відповіді на питання:

// 1.	1.	Опишіть своїми словами, що таке екранування і навіщо воно потрібно в мовах програмування.
  // Екранування – це спосіб виділення чогось( наприклад апостроф в українській або англійській мові), щоб цей символ не сприймався як кінець рядка( строки). За допомогою екранування ми також можемо застосовувати інші символи які будуть безпосередньо вказувати на якусь дію( наприклад перенос на нову строку  - /n)


// 2.	Які способи оголошення функцій ви знаєте?
  // Існує три способа оголошення функції в JS:
  // •	Function declaration – оголошується за допомогою ключового слова function, вона створюється до початку виконання JS коду, тому її можна викликати до того місця де вона була оголошена.
  // •	Function expression – функціональний вираз. Оголошується за допомогою ключового слова function, але вона не має свого власного імені і записується в змінну. Можна викликати лише після її оголошення в коді.
  // •	Named Function expression - оголошується за допомогою ключового слова function, а також має своє ім’я( правда її не можна викликати по цьому імені). Можна викликати лише після її оголошення в коді.
 

// 3.	Що таке hosting, як він працює для змінних та функцій?
    // Hosting  або підняття – це процес в JS при якому змінні та оголошення функцій піднімаються на верх в своїй області видимості перед тим як код буде виконано. 
    // Hosting для функцій.
    // Оголошення функції (Function declaration) підіймає її вверх над всіма змінними і саме це нам дозволяє використовувати функцію, до того як ми оголосимо її в своєму коді. Функції в JS  повністю підняті.
    // Hosting для змінних.
    // В JS hosting дозволяє використовувати зміні до їх оголошення. Підняття змінної в JS  стосується тільки оголошення змінної, а не її ініціалізації. Змінні в JS частково підняті. 
    // Var при піднятті буде ініціалізована з дефолтним значенням undefined. 
    // Let і const піднімаються, але не ініціалізуються, а спроба отримати до них доступ видасть Reference Error.


// Завдання №1

let firstName = prompt("Enter your first name: ");
let lastName = prompt("Enter your last name: ");
let birthday = prompt("Enter your birthday in fromat 'dd.mm.yyyy'", "dd.mm.yyyy");

function createNewUser(firstName, lastName, birthday) {
  return {
    firstName: firstName,
    lastName: lastName,
    birthday: birthday,
    getLogin: function () {
      let result = this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
      return result
    },
    getAge: function (){
      let myDate = this.birthday.split('.');
      myDate = new Date(myDate[2], myDate[1] - 1, myDate[0]);
      let nowDate = new Date();
      let birthdayDay = new Date(myDate);
      let ageUser = Math.floor((nowDate - birthdayDay) / (1000 * 60 * 60 * 24 * 365));
      return ageUser;
    },
    getPassword: function(){
      let password = this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substr(-4, 4);
      return password;
    },
  };
}
let newUser = createNewUser(firstName, lastName, birthday);

console.log(newUser);
// console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
